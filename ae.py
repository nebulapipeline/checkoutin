import uiContainer  # noqa: F401, E402
from PyQt4.QtGui import QApplication
import sys


import nebula.auth.user as user


def do():

    if not user.user_registered():

        import login
        if not login.Dialog().exec_():
            return

    import checkoutin

    global win
    win = checkoutin.MainBrowser(standalone=True)
    win.show()


newApp = QApplication(sys.argv)
do()
sys.exit(newApp.exec_())
