import uiContainer
from PyQt4.QtGui import QApplication, qApp
import sys

import nebula.auth.user as user


def do():
    
    if not user.user_registered():
        import login
        if not login.Dialog().exec_():
            return
    import checkoutin
    
    global win
    win = checkoutin.ShotExplorer(standalone=True)
    win.show()

newApp = QApplication(sys.argv)
do()
sys.exit(newApp.exec_())
